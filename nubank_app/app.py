# app.py

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'  # URI de conexão com o banco de dados SQLite
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

# Importar modelos de dados
# from api.conta.models import Conta
from api.conta.models import Conta, Transacao
# from api.transacao.models import Transacao

# Importar rotas
from api.conta.routes.conta_routes import conta_routes
from api.transacao.routes.transacao_routes import transacao_routes

# Registrar rotas
app.register_blueprint(conta_routes)
app.register_blueprint(transacao_routes)

if __name__ == '__main__':
    app.run(debug=True)
