# nubank_app/api/transacao/views.py

from flask import Blueprint, request, jsonify
from .controllers.transacao_controllers import registrar_transacao

transacao_blueprint = Blueprint('transacao', __name__)

@transacao_blueprint.route('/registrar_transacao', methods=['POST'])
def registrar_transacao_route():
    data = request.get_json()
    tipo = data.get('tipo')
    valor = data.get('valor')

    if not tipo or not valor:
        return jsonify({'error': 'Tipo ou valor da transação não fornecido'}), 400

    mensagem = registrar_transacao(tipo, valor)
    return jsonify({'mensagem': mensagem})
