# routes/transacao_routes.py

from flask import Blueprint, jsonify, request
from .controllers.transacao_controllers import registrar_transacao

transacao_routes = Blueprint('transacao_routes', __name__)

@transacao_routes.route('/registrar_transacao', methods=['POST'])
def registrar_transacao_route():
    data = request.get_json()
    tipo = data.get('tipo')
    valor = data.get('valor')

    if not tipo or not valor:
        return jsonify({'error': 'Tipo ou valor da transação não fornecido'}), 400

    return registrar_transacao(tipo, valor)
