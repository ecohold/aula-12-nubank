# controllers/transacao_controllers.py

from flask import jsonify, request
from .models import db, Conta, Transacao

def registrar_transacao(tipo, valor):
    conta = Conta.query.first()  # Supondo que há apenas uma conta no sistema

    if tipo not in ['saque', 'deposito']:
        return jsonify({'error': 'Tipo de transação inválido'}), 400

    if valor <= 0:
        return jsonify({'error': 'O valor da transação deve ser um número positivo'}), 400

    if tipo == 'saque':
        if valor <= conta.saldo:
            novo_saldo = conta.saldo - valor
            conta.saldo = novo_saldo
            db.session.add(Transacao(tipo=tipo, valor=valor, conta_id=conta.id))
            db.session.commit()
            return jsonify({'message': f'Saque de R${valor:.2f} realizado com sucesso. Novo saldo: R${novo_saldo:.2f}'}), 200
        else:
            return jsonify({'error': 'Saldo insuficiente para realizar o saque'}), 400

    elif tipo == 'deposito':
        novo_saldo = conta.saldo + valor
        conta.saldo = novo_saldo
        db.session.add(Transacao(tipo=tipo, valor=valor, conta_id=conta.id))
        db.session.commit()
        return jsonify({'message': f'Depósito de R${valor:.2f} realizado com sucesso. Novo saldo: R${novo_saldo:.2f}'}), 200
