# nubank_app/api/transacao/models.py

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Transacao(db.Model):
    __tablename__ = 'transacao'

    id = db.Column(db.Integer, primary_key=True)
    tipo = db.Column(db.String(10), nullable=False)
    valor = db.Column(db.Numeric(precision=10, scale=2), nullable=False)
    data = db.Column(db.DateTime, default=db.func.current_timestamp())
    conta_id = db.Column(db.Integer, db.ForeignKey('conta.id'), nullable=False)

    conta = db.relationship('Conta', backref=db.backref('transacoes', lazy=True))

    def __repr__(self):
        return f'<Transacao {self.id}, Tipo: {self.tipo}, Valor: {self.valor}, Data: {self.data}>'
