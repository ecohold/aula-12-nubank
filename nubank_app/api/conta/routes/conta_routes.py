# routes/conta_routes.py

from flask import Blueprint, jsonify, request
from ..controllers.conta_controllers import verificar_saldo_saque


conta_routes = Blueprint('conta_routes', __name__)

@conta_routes.route('/verificar_saldo_saque', methods=['POST'])
def verificar_saldo_saque_route():
    data = request.get_json()
    valor_sacado = data.get('valor_sacado')

    if not valor_sacado:
        return jsonify({'error': 'Valor de saque não fornecido'}), 400

    return verificar_saldo_saque(valor_sacado)
