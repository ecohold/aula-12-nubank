# nubank_app/api/conta/views.py

from flask import Blueprint, request, jsonify
from .controllers.conta_controllers import verificar_saldo_saque, realizar_deposito

conta_blueprint = Blueprint('conta', __name__)

@conta_blueprint.route('/verificar_saldo_saque', methods=['POST'])
def verificar_saldo_saque_route():
    data = request.get_json()
    valor_sacado = data.get('valor_sacado')

    if not valor_sacado:
        return jsonify({'error': 'Valor de saque não fornecido'}), 400

    mensagem = verificar_saldo_saque(valor_sacado)
    return jsonify({'mensagem': mensagem})

@conta_blueprint.route('/realizar_deposito', methods=['POST'])
def realizar_deposito_route():
    data = request.get_json()
    valor_deposito = data.get('valor_deposito')

    if not valor_deposito:
        return jsonify({'error': 'Valor de depósito não fornecido'}), 400

    mensagem = realizar_deposito(valor_deposito)
    return jsonify({'mensagem': mensagem})
