# nubank_app/api/conta/controllers/conta_controllers.py

# controllers/conta_controllers.py

from flask import jsonify, request
from .models import db, Conta, Transacao

def verificar_saldo_saque(valor_sacado):
    conta = Conta.query.first()  # Supondo que há apenas uma conta no sistema

    if valor_sacado <= 0:
        return jsonify({'error': 'O valor a ser sacado deve ser um número positivo'}), 400

    saldo_atual = conta.saldo

    if valor_sacado <= saldo_atual:
        novo_saldo = saldo_atual - valor_sacado
        conta.saldo = novo_saldo
        db.session.commit()
        return jsonify({'message': f'Saque de R${valor_sacado:.2f} realizado com sucesso. Novo saldo: R${novo_saldo:.2f}'}), 200
    else:
        if novo_saldo < 100:
            deposito_necessario = 100 - novo_saldo
            novo_saldo += deposito_necessario
            conta.saldo = novo_saldo
            db.session.commit()
            return jsonify({'message': f'Saldo mínimo abaixo do limite. Foi realizado um depósito de R${deposito_necessario:.2f}. Novo saldo: R${novo_saldo:.2f}'}), 200
        else:
            return jsonify({'error': 'Saldo insuficiente para realizar o saque'}), 400
