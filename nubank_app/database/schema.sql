-- Criação da tabela de contas
CREATE TABLE conta (
    id SERIAL PRIMARY KEY,
    saldo DECIMAL(10, 2) DEFAULT 1000.00
);

-- Criação da tabela de transações
CREATE TABLE transacao (
    id SERIAL PRIMARY KEY,
    tipo VARCHAR(10) NOT NULL,
    valor DECIMAL(10, 2) NOT NULL,
    data TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    conta_id INTEGER REFERENCES conta(id)
);
