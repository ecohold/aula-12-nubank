# aula-12-nubank - Educafrotech

# PROJETO DESAFIO FINAL – LÓGICA DE PROGRAMAÇÃO

![Alt text](image.png)


        ATIVIDADES:

        1)FAZER UM PROGRAMA PARA SABER SE TEM DINHEIRO DISPONÍVEL NA
        CONTA PARA SACAR OU NÃO.
        VALOR INICIAL NA CONTA: R$1000,00
        VALOR MÍNIMO DE SALDO NA CONTA: R$100,00
        DICAS PARA CRIAR O ALGORITMO:
            1 – ENTRAR COM O VALOR A SER SACADO.
            2 – SE O VALOR A SER SACADO FOR MENOR QUE O LIMITE DA CONTA,
            ESCREVA A MENSAGEM “SALDO DISPONÍVEL”, SENÃO ESCREVA “SALDO
            INSUFICIENTE” E EXIBIR O SALDO.
            3 – TODA VEZ QUE SACAR, DIMINUIR DO VALOR DO SALDO DA CONTA E
            MOSTRAR QUANTO FICOU PARA O PRÓXIMO SAQUE.
            4- SE O VALOR FOR MENOR QUE O VALOR MÍNIMO, EXIBIR A MENSAGEM
            “SALDO MÍNIMO ABAIXO DO LIMITE, POR GENTILEZA REALIZAR DEPÓSITO
            NA CONTA”. FAZER O DEPÓSITO PARA QUE A CONTA NÃO FIQUE NO
            NEGATIVO.
            5- ENTRE COM O VALOR A SER DEPOSITADO E SOME COM O VALOR DO
            SALDO EXISTENTE.


        2)ESCREVA UM PRGRAMA PARA EXIBIR MENU DE UM RESTAURANTE E
        CONFORME A OPÇÃO ESCOLHIDA, EXIBIR NA TELA O PRATO ESCOLHIDO E
        O SEU VALOR PARA DUAS PESSOAS.
        DICAS: MENU A SER MOSTRADO PARA A ESCOLHA:
            1 – LAGOSTA AO MOLHO ROSÉ – VALOR POR PESSOA:R$120,00
            2 – MOQUECA DE CAMARÃO – VALOR POR PESSOA:R$100,00
            3 – FILÉ DE PEIXE COM ARROZ BRANCO – VALOR POR PESSOA:R$50,00

                nubank_app/
        ├── api/
        │   ├── conta/
        │   │   ├── controllers/
        │   │   │   ├── conta_controllers.py
        │   │   │   └── ...
        │   │   ├── models.py
        │   │   ├── routes/
        │   │   │   ├── conta_routes.py
        │   │   │   └── ...
        │   │   └── views.py
        │   └── transacao/
        │       ├── controllers/
        │       │   ├── transacao_controllers.py
        │       │   └── ...
        │       ├── models.py
        │       ├── routes/
        │       │   ├── transacao_routes.py
        │       │   └── ...
        │       └── views.py

        Claro, antes de prosseguir, vamos documentar o projeto adequadamente, incluindo os requisitos. Aqui está o que você pode fazer:

1. **Criar o arquivo `requirements.txt`:** Este arquivo listará todas as dependências do projeto, para que outros desenvolvedores possam facilmente instalar as mesmas versões das bibliotecas que você usou.

   ```bash
   cd ~/Documents/aula-12-nubank/nubank_app
   pip freeze > requirements.txt
   ```

   Este comando irá criar um arquivo `requirements.txt` contendo todas as bibliotecas Python instaladas no ambiente virtual atual.

2. **Documentar o projeto:**
   - Descreva brevemente o propósito do projeto no arquivo README.md.
   - Forneça instruções claras sobre como configurar e executar o projeto.
   - Inclua uma breve descrição de cada diretório e arquivo importante no README.md.
   - Explique a estrutura do projeto e a organização dos arquivos.
   - Liste todas as dependências necessárias para o projeto e instruções para instalá-las.

3. **Atualizar o README.md:** Abra o arquivo README.md e adicione ou atualize todas as informações relevantes sobre o projeto.

   Por exemplo:

   ```markdown
   # Projeto Nubank API

   Este projeto é uma implementação de uma API simples para simular operações bancárias básicas, como verificação de saldo, saques, depósitos e registro de transações. Utiliza o framework Flask para criar a API e o SQLAlchemy para interagir com o banco de dados.

   ## Estrutura do Projeto

   O projeto está organizado da seguinte forma:

   - **api/**: Contém os módulos relacionados à API, incluindo contas e transações.
     - **conta/**: Módulo para funcionalidade relacionada a contas bancárias.
       - **controllers/**: Controladores para manipular a lógica de negócios das contas.
       - **models.py**: Definição dos modelos de dados relacionados às contas bancárias.
       - **routes/**: Rotas para manipular as solicitações relacionadas às contas.
       - **views.py**: Lógica para renderizar visualizações relacionadas às contas (se necessário).
     - **transacao/**: Módulo para funcionalidade relacionada a transações bancárias.
       - **controllers/**: Controladores para manipular a lógica de negócios das transações.
       - **models.py**: Definição dos modelos de dados relacionados às transações bancárias.
       - **routes/**: Rotas para manipular as solicitações relacionadas às transações.
       - **views.py**: Lógica para renderizar visualizações relacionadas às transações (se necessário).
   - **app.py**: Arquivo principal para iniciar o servidor Flask.
   - **database/**: Arquivo `schema.sql` que define a estrutura do banco de dados.
   - **imagens/**: Contém imagens relacionadas ao projeto.
   - **README.md**: Documentação do projeto.

   ## Requisitos

   - Python 3.x
   - Flask
   - Flask-SQLAlchemy
   - Outras dependências listadas em requirements.txt

   ## Configuração e Execução

   1. Clone o repositório:
      ```bash
      git clone https://gitlab.com/ecohold/aula-12-nubank

   2. Instale as dependências:
      ```bash
      pip install -r requirements.txt
      ```

   3. Execute o aplicativo:
      ```bash
      python3 app.py
      ```

   ## Exemplo de Uso

   - Endpoint para verificar saldo de uma conta:
     ```bash
     curl -X POST -H "Content-Type: application/json" -d '{"valor_sacado": 100}' http://localhost:9999/api/conta/verificar_saldo_saque
     ```

   ## Contribuição

   Contribuições são bem-vindas! Para contribuir com este projeto, siga estas etapas:
   1. Faça um fork do projeto
   2. Crie uma branch para sua feature (`git checkout -b feature/nova-feature`)
   3. Faça commit das suas mudanças (`git commit -am 'Adicione uma nova feature'`)
   4. Faça push para a branch (`git push origin feature/nova-feature`)
   5. Abra um Pull Request

   ```

4. **Commit e Push:** Depois de documentar o projeto, faça commit das alterações e envie para o repositório remoto:

   ```bash
   git add .
   git commit -m "Adiciona documentação e requirements.txt"
   git push origin main
   ```

Com esses passos, seu projeto estará bem documentado e pronto para ser compartilhado e colaborado por outros desenvolvedores.

# -----------------refatora------------------ # 

Árvore de Arquivos Completa
Data: 26 de março de 2024
Versão: 1.0

Estrutura:

      ├── app/
      │   ├── Dockerfile
      │   ├── README.md
      │   ├── package.json
      │   ├── **index.js**
      │   ├── **server.js**
      │   ├── **config.js**
      │   ├── **routes.js**
      │   ├── **controllers/**
      │   │   ├── **Api1Controller.js**
      │   │   └── **Api2Controller.js**
      │   ├── **models/**
      │   │   ├── **User.js**
      │   │   └── **Product.js**
      │   ├── **services/**
      │   │   ├── **AuthService.js**
      │   │   └── **DatabaseService.js**
      │   ├── **utils/**
      │   │   ├── **api.js**
      │   │   └── **storage.js**
      │   ├── **database/**
      │   │   ├── **migrations/**
      │   │   │   ├── **001_create_users_table.js**
      │   │   │   └── **002_create_products_table.js**
      │   │   └── **seeds/**
      │   │       ├── **users.js**
      │   │       └── **products.js**
      │   └── **assets/**
      │       ├── **fonts/**
      │       ├── **images/**
      │       └── **scripts/**
      └── nginx.conf
Funções dos Arquivos:

Dockerfile: Define o ambiente de desenvolvimento e produção da aplicação.
README.md: Documentação do projeto.
package.json: Gerenciador de dependências do Node.js.
index.js: Arquivo principal da aplicação.
server.js: Configura o servidor da aplicação.
config.js: Configurações da aplicação, como porta do servidor, banco de dados, etc.
routes.js: Define as rotas da API da aplicação.
Controllers: Contêm a lógica das APIs da aplicação.
Models: Definem os modelos de dados da aplicação.
Services: Contêm os serviços da aplicação, como autenticação e acesso ao banco de dados.
Utils: Ferramentas e utilitários da aplicação.
Database: Migrações e dados iniciais do banco de dados.
Assets: Fontes, imagens e scripts da aplicação.
nginx.conf: Configura o servidor web Nginx.
Observações Adicionais:

Esta estrutura de arquivos é apenas um exemplo e pode ser adaptada às necessidades do seu projeto.
Você pode adicionar ou remover arquivos e pastas conforme necessário.
É importante manter a organização e a documentação do seu projeto para facilitar a manutenção e o desenvolvimento futuro.
Recursos Adicionais:


# ---------------manutenção-----------------#


Estrutura do código para um aplicativo React com back-end Node.js

Imagem 1: Estrutura do front-end

         app/
         ├── front-end/
         │  ├── componentes/
         │  │  ├── atoms/ (Elementos básicos como botões, inputs)
         │  │  │  ├── Button.js
         │  │  │  ├── Input.js
         │  │  │  └── Icon.js
         │  │  ├── molecules/ (Componentes combinando elementos básicos)
         │  │  │  ├── Card.js
         │  │  │  └── Modal.js
         │  │  ├── organisms/ (Componentes mais complexos)
         │  │  │  ├── Header.js
         │  │  │  └── Footer.js
         │  │  ├── templates/ (Layouts de página)
         │  │  │  ├── Home.js
         │  │  │  └── Profile.js
         │  │  ├── containers/ (Componentes que lidam com busca de dados e lógica)
         │  │  │  ├── HomeContainer.js
         │  │  │  └── ProfileContainer.js
         │  │  ├── pages/ (Pontos de entrada para rotas de nível superior)
         │  │  │  ├── Home.js
         │  │  │  └── Profile.js
         │  │  ├── styles/ (CSS para estilo)
         │  │  │  ├── main.css
         │  │  │  └── theme.js
         │  │  └── utils/ (Funções utilitárias)
         │  │      ├── api.js (Chamadas de API)
         │  │      └── storage.js (Armazenamento local)
         │  │  ├── index.html (Arquivo HTML principal)
         │  │  ├── package.json (Dependências do projeto)
            │  │  └── README.md (Documentação do projeto)
            │  ├── assets/ (Recursos estáticos como imagens e fontes)
            │  │  ├── fonts/
            │  │  ├── images/
            │  │  └── scripts/
            │  ├── Dockerfile (Instruções de construção para a imagem front-end)
            │  ├── docker-compose.yml (Configuração para executar o front-end com docker-compose)
            │  └── nginx.conf (Configuração opcional do Nginx para proxy reverso)


Imagem 2: Estrutura do back-end

         app/
         ├── back-end/
         │  ├── config/ (Arquivos de configuração)
         │  │  ├── database.js (Conexão com banco de dados)
         │  │  └── server.js (Inicialização do servidor)
         │  ├── controllers/ (Lógica de endpoint de API)
         │  │  ├── Api1Controller.js
         │  │  └── Api2Controller.js
         │  ├── middlewares/ (Funções para modificar requisições e respostas)
         │  │  ├── auth.js (Middleware de autenticação)
         │  │  └── validation.js (Middleware de validação de requisição)
         │  ├── models/ (Modelos de dados representando tabelas do banco de dados)
         │  │  ├── User.js
         │  │  └── Product.js
         │  ├── routes/ (Mapeando URLs para controladores)
         │  │  ├── api1.js
         │  │  └── api2.js
         │  └── services/ (Funções de lógica de negócios)
         │      ├── AuthService.js (Serviço de autenticação)
         │      └── DatabaseService.js (Serviço de interação com banco de dados)
         │  ├── index.js (Ponto de entrada para back-end)
         │  ├── package.json (Dependências do projeto)
         │  └── README.md (Documentação do projeto)
         │  ├── Dockerfile (Instruções de construção para a imagem back-end)
         │  ├── docker-compose.yml (Configuração para executar o back-end com docker-compose)
         │  └── nginx.conf (Configuração opcional do Nginx para proxy reverso)


# --------------- testes ----------------#

## Estrutura de Pastas Atualizada com Testes e Ferramentas

**Imagem 1: Front-end**

```
            app/
            ├── front-end/
            │ ├── __tests__/
            │ │   ├── unit/
            │ │   │   ├── atoms/
            │ │   │   │   ├── Button.test.js
            │ │   │   │   └── Input.test.js
            │ │   │   ├── molecules/
            │ │   │   │   ├── Card.test.js
            │ │   │   │   └── Modal.test.js
            │ │   │   ├── organisms/
            │ │   │   │   ├── Header.test.js
            │ │   │   │   └── Footer.test.js
            │ │   │   ├── containers/
            │ │   │   │   ├── HomeContainer.test.js
            │ │   │   │   └── ProfileContainer.test.js
            │ │   │   └── integration/
            │ │   │       ├── Home.test.js
            │ │   │       └── Profile.test.js
            │ │   └── e2e/
            │ │       ├── cypress/
            │ │       │   └── integrations/
            │ │       │       ├── Home.spec.js
            │ │       │       └── Profile.spec.js
            │ ├── componentes/
            │ │ ├── atoms/
            │ │ │ ├── Button.js
            │ │ │ ├── Input.js
            │ │ │ └── Icon.js
            │ │ ├── molecules/
            │ │ │ ├── Card.js
            │ │ │ └── Modal.js
            │ │ ├── organisms/
            │ │ │ ├── Header.js
            │ │ │ └── Footer.js
            │ │ ├── templates/
            │ │ │ ├── Home.js
            │ │ │ └── Profile.js
            │ │ ├── containers/
            │ │ │ ├── HomeContainer.js
            │ │ │ └── ProfileContainer.js
            │ │ ├── pages/
            │ │ │ ├── Home.js
            │ │ │ └── Profile.js
            │ │ ├── styles/
            │ │ │ ├── main.css
            │ │ │ └── theme.js
            │ │ └── utils/
            │ │   ├── api.js (Chamadas de API)
            │ │   └── storage.js (Armazenamento local)
            │ │ ├── index.html (Arquivo HTML principal)
            │ │ ├── package.json (Dependências do projeto)
            │ │ └── README.md (Documentação do projeto)
            │ ├── assets/
            │ │ ├── fonts/
            │ │ ├── images/
            │ │ └── scripts/
             │ ├── Dockerfile (Instruções de construção para a imagem front-end)
             │ ├── docker-compose.yml (Configuração para executar o front-end com docker-compose)
             │ └── nginx.conf (Configuração opcional do Nginx para proxy reverso)

            ```

            **Imagem 2: Back-end**

            ```
            app/
            ├── back-end/
            │ ├── __tests__/
            │ │   ├── unit/
            │ │   │   ├── controllers/
            │ │   │   │   ├── Api1Controller.test.js
            │ │   │   │   └── Api2Controller.test.js
            │ │   │   ├── middlewares/
            │ │   │   │   ├── auth.test.js
            │ │   │   │   └── validation.test.js
            │ │   │   ├── models/
            │ │   │   │   ├── User.test.js
            │ │   │   │   └── Product.test.js
            │ │   │   └── services/
            │ │   │       ├── AuthService.test.js
            │ │   │       └── DatabaseService.test.js
            │ │   └── integration/
            │ │       ├── api/
            │ │       │   ├── api1.test.js
            │ │       │   └── api2.test.js
            │ │       └── database/
            │ │          ├── database.test.js
            │ │  



# ----------------construção -------------------3

# Imagem Docker - conteinerização

**Estrutura do código:**

      ```
      app/
      ├── front-end/
      │  ... (Estrutura do front-end conforme imagem 1)
      │  ├── Dockerfile
      │  ├── docker-compose.yml
      │  └── nginx.conf
      ├── back-end/
      │  ... (Estrutura do back-end conforme imagem 2)
      │  ├── Dockerfile
      │  ├── docker-compose.yml
      │  └── nginx.conf
      ```

**Dockerfile - Front-end:**

```dockerfile
FROM node:16-alpine

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

CMD ["npm", "start"]
```

**Dockerfile - Back-end:**

```dockerfile
FROM node:16-alpine

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

CMD ["npm", "start"]
```

**docker-compose.yml:**

      ```yaml
      version: "3.9"

      services:
      front-end:
         build: ./front-end
         ports:
            - "3000:3000"
      back-end:
         build: ./back-end
         ports:
            - "8080:8080"
         depends_on:
            - front-end

      networks:
      default:
         external:
            name: my-network
      ```

**nginx.conf (Opcional):**

         ```conf
         server {
         listen 80;
         server_name localhost;

         location / {
            proxy_pass http://front-end:3000;
         }

         location /api {
            proxy_pass http://back-end:8080;
         }
         }
         ```

**Observações:**

* Adapte os Dockerfiles e o `docker-compose.yml` de acordo com as suas necessidades.
* O `nginx.conf` é opcional e pode ser usado para configurar um proxy reverso.
* Certifique-se de ter instalado o Docker e o Docker Compose.
* Para executar o aplicativo, execute o comando `docker-compose up` na pasta `app`.
* O front-end estará disponível em `http://localhost:3000` e o back-end em `http://localhost:8080`.

**Integração Front-end e Back-end:**

* A integração entre o front-end e o back-end é feita através de APIs.
* O front-end faz requisições para as APIs do back-end para obter dados e enviar informações.
* O back-end é responsável por processar as requisições do front-end e retornar os dados ou realizar as ações solicitadas.

**Exemplo de integração:**

* O front-end possui um botão que, ao ser clicado, faz uma requisição para a API `/api/users` do back-end.
* O back-end processa a requisição e retorna uma lista de usuários.
* O front-end recebe a lista de usuários e a exibe na tela.

**Recursos adicionais:**

* Documentação do Docker: [https://docs.docker.com/get-started/](https://docs.docker.com/get-started/)
* Documentação do Docker Compose: [https://docs.docker.com/compose/](https://docs.docker.com/compose/)
* Documentação do Nginx: [https://nginx.org/en/docs/](https://nginx.org/en/docs/)

**Dicas:**

* Utilize variáveis de ambiente para configurar as URLs das APIs e outros parâmetros importantes.
* Implemente autenticação e autorização nas APIs do back-end.
* Utilize um sistema de cache para armazenar dados frequently accessed.
* Monitore o desempenho do seu aplicativo com ferramentas como o Prometheus e o Grafana.

**Padrão Senior Devops:**

* Este padrão visa facilitar o desenvolvimento e a implantação de aplicações em nuvem.
* O padrão utiliza containers Docker para encapsular o front-end e o back-end.
* O `docker-compose` é utilizado para gerenciar os containers.
* O Nginx pode ser usado como proxy reverso.
* As APIs do back-end são configuradas com autenticação e autorização.
* O sistema é monitorado com ferramentas como o Prometheus e o Grafana.


# -------------- requisitos de negocio ---------------- #


## UX - Requisitos de Negócio e Regras de Negócio

**Ótica do Usuário:**

* **Intuitivo e amigável:** Interface simples, clara e fácil de navegar, com linguagem acessível e ícones intuitivos.
* **Personalizável:** Opções para personalizar a experiência do usuário, como temas, cores e configurações de notificações.
* **Eficiente:** Tarefas e transações realizadas de forma rápida e eficiente, com tempos de resposta rápidos e sem travamentos.
* **Confiável:** Segurança robusta para proteger dados confidenciais do usuário, com criptografia de ponta e autenticação multifator.
* **Informativo:** Apresentação de informações relevantes e personalizadas de forma clara e concisa, com gráficos e tabelas intuitivas.
* **Acessível:** Compatibilidade com diferentes dispositivos e plataformas (mobile, web e desktop), com recursos de acessibilidade para pessoas com deficiência.

**Ótica Febraban:**

* **Cumprimento de normas e regulamentações:** Conformidade com todas as normas e regulamentações do Banco Central do Brasil e da Febraban, incluindo segurança de dados, prevenção à lavagem de dinheiro e proteção ao consumidor.
* **Integração com o sistema financeiro nacional:** Interoperabilidade com outros bancos e instituições financeiras, permitindo transações instantâneas e seguras.
* **Segurança robusta:** Proteção contra fraudes e ataques cibernéticos, com medidas de segurança rígidas e monitoramento constante.
* **Transparência:** Prestação de contas clara e transparente para os usuários, com informações detalhadas sobre transações, taxas e tarifas.

**Ótica do Mercado Financeiro Local e Global:**

* **Competitivo:** Oferecer produtos e serviços competitivos em termos de preço, qualidade e funcionalidade, atendendo às necessidades do mercado local e global.
* **Inovador:** Implementar novas tecnologias e soluções inovadoras que melhorem a experiência do usuário e a eficiência das operações.
* **Globalizado:** Suporte a transações internacionais em diferentes moedas e idiomas, com taxas competitivas e conversão cambial automática.
* **Regulatório:** Conformidade com as normas e regulamentações dos mercados financeiros locais e globais, incluindo KYC, AML e GDPR.

**Ótica do Nubank Gestor:**

* **Visão geral:** Painéis de controle intuitivos com informações detalhadas sobre o desempenho do aplicativo, incluindo número de usuários, transações realizadas, saldo em conta e outros indicadores relevantes.
* **Gestão de usuários:** Ferramentas para gerenciar o acesso e as permissões dos usuários, incluindo criação de perfis, bloqueio de contas e recuperação de senhas.
* **Análise de dados:** Ferramentas avançadas de análise de dados para identificar tendências, oportunidades e áreas de melhoria no aplicativo.
* **Gestão de riscos:** Monitoramento constante de riscos e fraudes, com ferramentas para identificar e prevenir atividades suspeitas.

**Ótica do Nubank Desenvolvedor Front-End e Back-End:**

**Front-End:**

* **Componentes reutilizáveis:** Biblioteca de componentes reutilizáveis para agilizar o desenvolvimento e garantir a consistência da interface.
* **Código limpo e organizado:** Estrutura de arquivos organizada e código bem documentado para facilitar a leitura e a manutenção.
* **Tecnologias modernas:** Uso de tecnologias front-end modernas como React, Vue.js ou Angular para garantir um desempenho otimizado e uma experiência do usuário aprimorada.
* **Acessibilidade:** Implementação de recursos de acessibilidade para garantir que o aplicativo seja acessível a todos os usuários, incluindo pessoas com deficiência.

**Back-End:**

* **Escalável e confiável:** Arquitetura robusta e escalável para lidar com grande volume de usuários e transações.
* **Segurança robusta:** Implementação de medidas de segurança rígidas para proteger dados confidenciais dos usuários.
* **Monitoramento constante:** Monitoramento constante do desempenho do aplicativo para identificar e solucionar problemas rapidamente.
* **Testes automatizados:** Testes automatizados para garantir a qualidade do código e a confiabilidade do aplicativo.

**Observações:**

* As listas acima não são exaustivas e podem ser complementadas de acordo com as necessidades específicas do projeto.
* A priorização dos requisitos de negócio deve ser feita em conjunto com as partes interessadas do projeto.

## Entregas do App Nubank Completo 

**Desenvolvedor:** Senior DevOps
**Data:** 26 de março de 2024
**Linguagem:** Português

**Resumo:**

O App Nubank Completo é um projeto que visa fornecer uma estrutura completa para o desenvolvimento de aplicações web complexas e escaláveis na nuvem. O projeto é dividido em duas partes principais:

